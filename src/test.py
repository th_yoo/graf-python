#!/usr/bin/env python
# vim: sw=4 ts=4 et fileencoding=utf-8

import graf

def main():
    p = graf.GraphParser()
    g = p.parse('data/wsj_0006.hdr')

    for node in g.nodes:
        print node


if __name__=='__main__':
    main()

